package cz.morosystems.service;

import cz.morosystems.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User save(User user);

    Optional<User> findById(Long id);

    User findByUsername(String username);

    List<User> findAll();

    void delete(User user);

}
