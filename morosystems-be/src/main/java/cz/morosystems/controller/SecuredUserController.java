package cz.morosystems.controller;

import cz.morosystems.exception.ResourceNotFoundException;
import cz.morosystems.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class SecuredUserController {

    @Autowired
    private UserService userService;


    @DeleteMapping("/secured/deleteUser/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable Long userId) {
        log.info("Deleting user with id " + userId);
        return userService.findById(userId)
                .map(user -> {
                    userService.delete(user);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("User not " +
                        "found with id " + userId));
    }

}

