package cz.morosystems.controller;

import cz.morosystems.exception.ResourceNotFoundException;
import cz.morosystems.model.User;
import cz.morosystems.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET,
            produces = "application/json")
    public User getUserById(@PathVariable Long userId) {
        log.info("Getting user with id " + userId);
        return userService.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User " +
                        "not found with id " + userId));
    }


    @RequestMapping(value = "/users", method = RequestMethod.GET,
            produces = "application/json")
    public List<User> getUsers() {
        log.info("Getting all users");
        return userService.findAll();
    }


    @PostMapping("/users")
    public User createUser(@Valid @RequestBody User user) {
        log.info("Creating new user");
        return userService.save(user);
    }

    @PutMapping("/users/{userId}")
    public User updateUser(@PathVariable Long userId,
                           @Valid @RequestBody User userRequest) {
        log.info("Updating existing user with id " + userId);
        return userService.findById(userId)
                .map(user -> {
                    user.setName(userRequest.getName());
                    return userService.save(user);
                }).orElseThrow(() -> new ResourceNotFoundException("User not " +
                        "found with id " + userId));
    }


}

