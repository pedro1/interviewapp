package cz.morosystems;

import cz.morosystems.model.User;
import cz.morosystems.repository.UserRepository;
import cz.morosystems.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.util.Properties;

@SpringBootApplication
public class MoroAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoroAppApplication.class, args);
    }

    @Bean
    public CommandLineRunner demoData(UserService repo) {
        return args -> {
            Properties users = PropertiesLoaderUtils.loadAllProperties(
                    "config.properties");
            String pwd = users.getProperty("password");

            repo.save(new User("Ivan", "ivan", pwd));
            repo.save(new User("Milan", "milan", pwd));
            repo.save(new User("Peter", "peter", pwd));
            repo.save(new User("Juraj", "juraj", pwd));
            repo.save(new User("Andrej", "andrej", pwd));
            repo.save(new User("Vaclav", "vaclav", pwd));
            repo.save(new User("Iveta", "iveta", pwd));
            repo.save(new User("Sona", "sona", pwd));
            repo.save(new User("Klaudia", "klaudia", pwd));
            repo.save(new User("Barbora", "barbora", pwd));
            repo.save(new User("Natalia", "natalia", pwd));
            repo.save(new User("Jurgen", "jurgen", pwd));
            repo.save(new User("Michal", "michal", pwd));
            repo.save(new User("Dominik", "dominik", pwd));
            repo.save(new User("Simon", "simon", pwd));
            repo.save(new User("Patrik", "patrik", pwd));
            repo.save(new User("Tomas", "tomas", pwd));
            repo.save(new User("Cyril", "cyril", pwd));
        };
    }

}
