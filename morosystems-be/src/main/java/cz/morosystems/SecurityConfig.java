package cz.morosystems;

import cz.morosystems.security.SavedRequestAwareAuthenticationSuccessHandler;
import cz.morosystems.security.entrypoint.JsonBasicAuthenticationEntryPoint;
import cz.morosystems.security.entrypoint.RestAuthenticationEntryPoint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

@EnableWebSecurity
@Slf4j
public class SecurityConfig {

    @Configuration
    public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

        @Autowired
        private JsonBasicAuthenticationEntryPoint basicAuthenticationEntryPoint;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable()
                    .httpBasic()
                    .authenticationEntryPoint(basicAuthenticationEntryPoint)
                    .and()
                    .authorizeRequests()
                    .antMatchers("/secured/**").authenticated()
                    .antMatchers("/**").permitAll();
        }
    }

//    @Configuration
//    @Order(2)
//    public static class LoginConfigurationAdapter extends WebSecurityConfigurerAdapter {
//
//        @Autowired
//        private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
//
//        @Autowired
//        private SavedRequestAwareAuthenticationSuccessHandler successHandler;
//
//        private SimpleUrlAuthenticationFailureHandler failureHandler = new SimpleUrlAuthenticationFailureHandler();
//
//        @Override
//        protected void configure(HttpSecurity http) throws Exception {
//            http.csrf().disable()
//                    .exceptionHandling()
//                    .authenticationEntryPoint(restAuthenticationEntryPoint)
//                    .and()
//                    .authorizeRequests()
//                    .antMatchers("/secured/**").authenticated()
//                    .antMatchers("/**").permitAll()
//                    .and()
//                    .formLogin()
//                    .successHandler(successHandler)
//                    .failureHandler(failureHandler);
//        }
//    }


    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }
}