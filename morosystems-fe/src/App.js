import React from 'react';
import './App.css';

import request from 'superagent';

import Table from './components/Table';
import TextField from './components/TextField';


class App extends React.Component {
 
  constructor(props)
  {
    super(props);
    this.state = {
      userData: []
    };
    this.handleDeleteUser = this.handleDeleteUser.bind(this);
    this.handleCreateUser = this.handleCreateUser.bind(this);
  }


  handleDeleteUser(id) {
    request
       .del('/secured/deleteUser/' + id)
       .auth('ivan', 'heslo')
       .set('Accept', 'application/json')
       .then(res => {
            console.log("Deleting user with id " + id + " was successful", res);
            // Array.prototype.filter returns new array
            // so we aren't mutating state here
            const arrayCopy = this.state.userData.filter((user) => user.id !== id);
            this.setState({userData: arrayCopy});
       })
       .catch(err => {
          console.log("An error occured while deleting user with id " + id + ".", err);
       });
  }


  handleCreateUser = (name) => {
    request
      .post('/users')
      .send({ name: name })
      .set('Accept', 'application/json')
      .then(res => {
        console.log("Creating user with name " + name + " was successful", res);
        const arrayCopy = this.state.userData.slice();
        arrayCopy.push({id: res.body.id, name: res.body.name, username: res.body.username});
        this.setState({userData: arrayCopy});
      })
      .catch(err => {
        console.log("An error occured while creating user with name " + name + ".", err);
      });


    
  };

  loadUsers()
  {
    request
    .get('/users')
    .then(res => {
        console.log("Getting users from server was succesful.", res.body, res.headers, res.status);
        const users = res.body;
        this.setState({
          userData: users
        })
    })
    .catch(err => {
      console.log("An error occured while getting users.", err);
    });
  }

  componentDidMount()
  {
    this.loadUsers();
  }
  
  render() {
    
    const headings = [
      "ID",
      "Meno",
      "Užívateľské meno",
      "Akcie"
    ];
   
    return (
      <div>
        <Table 
                  headings={headings}
                  data={this.state.userData}
                  onRowDeleted={this.handleDeleteUser}
                  />
        <TextField
                  onCreateUser={this.handleCreateUser}/>
      </div>
      
    );

  }
}

export default App;
