import * as React from 'react';
import PropTypes from 'prop-types';



export default class TextField extends React.Component {
    constructor(props) {
      super(props);
      this.state = {value: ''};
  
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);
    }
  
    handleChange(event) {
      this.setState({value: event.target.value});
    }
  
    handleSubmit(event) {
    //   alert('A name was submitted: ' + this.state.value);
      // add new user to data   
      this.props.onCreateUser(this.state.value);
      this.setState({value: ''});
      event.preventDefault();
    }

  
    render() {
      return (
        <form onSubmit={this.handleSubmit}>
            <label>Vytvorte nového užívateľa:</label><br/>
            <input type="text" placeholder="Zadajte meno" value={this.state.value} onChange={this.handleChange} />
            <button type="submit">Pridať</button>
        </form>
      );
    }
  }



TextField.propTypes = {
    
};
