import * as React from 'react';
import PropTypes from 'prop-types';

import Row from './Row';

const Body = props => {

  return (
      props.data.map((rowData) => 
        <Row 
            key={`row-${rowData.id}`}
            data={rowData} 
            onRowDeleted={props.onRowDeleted} />) 
  );
}

Row.propTypes = {
    data: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        username: PropTypes.string.isRequired
    }).isRequired,
  };

export default Body;