import * as React from 'react';
import PropTypes from 'prop-types';

import Cell from './Cell';

const Row = props => {

    const rowData = props.data;

    return (
        <tr>
            {
                Object.keys(rowData).map((key, cellIndex) => {
                    return (
                        <Cell
                            key={`${rowData.id}-${cellIndex}`}
                            content={rowData[key]}
                            header={false}
                            />
                    );
                })
            }
            <Cell
                content={
                    <div className="actions">
                        <button onClick={() => props.onRowDeleted(rowData.id)}>Zmazať</button>
                    </div>
                }
                header={false}
                />
        </tr>
      )
}

Row.propTypes = {
    data: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        username: PropTypes.string.isRequired
    }).isRequired,
  };

export default Row;