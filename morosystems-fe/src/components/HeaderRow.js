import * as React from 'react';
import PropTypes from 'prop-types';

import Cell from './Cell';

const HeaderRow = props => {

    return (
        <tr key="heading">
          {props.headings.map((heading, index) => {
            return (
              <Cell
                key={`heading-${index}`}
                content={heading}
                header={true}
              />
            )
          })}
      </tr>
    );
}

HeaderRow.propTypes = {
  headings: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default HeaderRow;