import * as React from 'react';
import PropTypes from 'prop-types';

const Cell = props => {

  const cellMarkup = props.header ? (
    <th className="Cell Cell-header">
      {props.content}
    </th>
  ) : (
    <td className="Cell">
      {props.content}
    </td>
  );

  return (cellMarkup);
}

Cell.propTypes = {
    header: PropTypes.bool.isRequired
  };

export default Cell;