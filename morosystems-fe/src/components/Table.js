import * as React from 'react';
import PropTypes from 'prop-types';

import HeaderRow from './HeaderRow';
import Body from './Body';


const Table = props => {
    
    return (    
        <table className="Table">
            <tbody>
                <HeaderRow headings={props.headings}/>
                <Body data={props.data} onRowDeleted={props.onRowDeleted}/>
            </tbody>
        </table>
    );
}

Table.propTypes = {
    headings: PropTypes.arrayOf(PropTypes.string).isRequired,
    data: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
      })).isRequired,
  };

export default Table;